#Instructions:
#   Author modifies the funciton writeQA avoiding the magic words:    
#   firstRendition     insertImage      questionString
#   answer2question    units2answer    prefix2answer    
# As per Python rules, all commands in the function function be indented

####  scroll down to def writeQA(FirstRendition)   #########

import os, csv, re, time, shutil, random, sys, math
random.seed() #needed to make random number different each time the code opens
def startLatex(author, attribution, about):    
    questionName=os.path.basename(sys.argv[0][:-3])
    bs=r'\newcommand{\questionName}{'+questionName+'}\n'
    bs+=r'''\newcommand{\quiztype}{numerical}
\newif\ifkey \keytrue \documentclass[11pt]{exam}
\RequirePackage{amssymb, amsfonts, amsmath, latexsym, verbatim,xspace, setspace,datetime}
\RequirePackage{tikz, pgflibraryplotmarks, hyperref,textcomp}
\usepackage[left=.5in, right=.5in, bottom=.5in, top=.75in]{geometry}
\usepackage{endnotes, multicol,textgreek} \usepackage{graphicx} 
\singlespacing  \parindent 0ex \hypersetup{ colorlinks=true, urlcolor=blue}
\begin{document}\title{\questionName}
\author{Question author: '''+author+r''' \\
''' + 'License: ' + attribution+r'''\\
LaTex code that generate this question is released to the Public Domain}
''' + r'''\maketitle\begin{center}                                                                                
\includegraphics[width=0.15\textwidth]{images/666px-Wikiversity-logo-en.png}
\\For more information visit:\\
\footnotesize{
    \url{https://en.wikiversity.org/wiki/Quizbank}\\
    \url{https://bitbucket.org/Guy_vandegrift/qbwiki/wiki/Home}}
\end{center}'''
    bs+='\n'+about+'\n'
    bs+='\\printanswers\\begin{questions}\n\\question'
    return bs
def roundSigFig(x, sigFig):
    #rounds positive or negative x to sigFig figures
    from math import log10, floor
    return round(x, sigFig-int(floor(log10(abs(x))))-1)
class QuesVar:
    #creates random input variables x.v(t) is number(text)
    def __init__(self, first, low, high, firstRendition):
        sigFigQues=3
        self.precision=3 #Number of digits to round
        self.first=first
        # see if either low or high are floating
        oneFloats=isinstance(low,float) or isinstance(high,float)
        if firstRendition:
            temp=first #temp is the local variable for self.v
            self.v=temp #v=value
        else:  #it is not the first question and may or may not float
            if oneFloats: #select a random floating value
                temp=random.uniform(low,high)
                temp=roundSigFig(temp,sigFigQues)
                self.v=temp
            else:
                temp=random.randint(low,high)
                temp=roundSigFig(temp,sigFigQues)
                self.v=temp
        #tempV=self.v and is used to create self.t (text version
        if abs(temp) > .01 and abs(temp) < 99:
            self.t=str(temp)
        else:
            formatStatement='{0:1.'+str(sigFigQues)+'E}'
            self.t=formatStatement.format(temp)
def createFootnote(author,attribution):
    s='\n\\ifkey\\endnote{Question licensed by '
    s+=author+' under Creative Commons '+attribution
    s+=r'}\else{}\fi'
    return s
def makeAnswers(prefix2answer, answer2question,
            units2answer,detractorsOffBy,offByFactors):
    sigFigAns=2
    s4mat="{:."+str(sigFigAns)+"E}" #string format
    offBy=detractorsOffBy #separation between answers
    nCorrect=random.randint(0,4)
    s='\n\\begin{choices}\n'
    for n in range(5):
        if n==nCorrect:           
            s+='\\CorrectChoice '+prefix2answer
            s+=s4mat.format(answer2question)
            s+=units2answer+'\n'
        else:
            s+='\\choice '+prefix2answer
            if not offByFactors:
                error=offBy*(n-nCorrect)
                thisAnswer=answer2question+error
                s+=s4mat.format(thisAnswer)
                s+=units2answer+'\n'
            if offByFactors:
                error=offBy**(n-nCorrect)
                thisAnswer=answer2question*error
                s+=s4mat.format(thisAnswer)
                s+=units2answer+'\n'              
    s+='\\end{choices}\n'
    return s
def finishLatex():
    #Creates the latex .tex file
    firstRendition=True
    [questionString,prefix2question,answer2question,
     units4answer,insertImage,
     author,attribution,about]=writeQA(firstRendition)
    bs=startLatex(author,attribution, about)+'\n'+questionString
    firstRendition=False
    bs+='\n\\keyfalse\n'
    for count in range(19):
        bs+='\n\\question\n'
        [questionString,prefix2question,answer2question,
         units4answer,insertImage,
         author,attribution,about]=writeQA(firstRendition)
        bs+=questionString
    bs+='\n\\end{questions}\n\\theendnotes\\end{document}'
    with open(os.path.basename(sys.argv[0][:-3])+'.tex','w') as fout:
        fout.write(bs)
    with open(os.path.basename(sys.argv[0][:-3])+'.txt','w') as fout:
        fout.write(bs)
        
##########################################################################        

########       AUTHOR OF NEW QUESTION STARTS HERE             ############
        
##########################################################################

       
def writeQA(firstRendition):
    
    #Step 1: Fill in author, attribution, and short explanation
    
    author=r'''OpenStax College University Physics'''
    attribution=r'''CC-BY copyright information available at \\
    \url{https://cnx.org/contents/1Q9uMg\_a@12.3:Gofkr9Oy@20/Preface}'''
    about=r'''We are grateful to David Marasco and Annie Chase
of Foothill College Physics. Their effort greatly facilitated
this attempt to turn the odd problems in OpenStax
University physics into a question bank that is also
an open educational resource'''
    
    #Step 2: To insert image, change False to True, add width and image name:
    
    insertImage=[False, 0.3,
         'Roller_coaster_energy_conservation.png']
    # The "images" folder must contain this image file

    #Step 3: Declare variables/image using the QuesVar class:
    
    a=   QuesVar(3.8, 3.8, 3.8, firstRendition)#
    F=   QuesVar(60, 11, 11, firstRendition)#
    m=   QuesVar(11, 11, 11, firstRendition)#
     
    if insertImage[0]:   #This inserts image, but only if requested
        questionString='\\includegraphics[width='+str(insertImage[1])+\
           '\\textwidth]{images/'+insertImage[2]+r'}\\'+'\n'
        #the standard is for the first line to be the image
    else:
        questionString='' #Initializes question if no image is needed

    #Step 4: Write the question
    questionString+='''A student\' backpack, full of textbooks,
is hung from a spring scale attached to the ceiling of an
elevator.  When the elevator is accelerating downward at
'''+a.t+''' $m/s^2$, the scale reads '''+F.t+''' N. What will
the scale read when the elevator later accelerates upward at the
same acceleration (of '''+a.t+r''' $m/s^2$)? Note: the folks at Foothill
claim the book\'s answer is wrong.'''
    
    #Step 5: Solve the problem, defining (non-magic) variables as needed:
    #xx=xxx.v

    
    #Step 6: Use magic words to state answer, units (if needed)
    
    prefix2answer=""# Or set to "" (empty string)
    answer2question = 98 #save 98
    units2answer =" N" #Blank to "" if dimensionless

    #Step 7 (optional): Adjust the wrong answers (called detractors)

    #offByFactors is usually True: causing the  RATIO of two consecutive
    #detractors to equal "detractorsOffBy"
    offByFactors=True
    detractorsOffBy=1.08
    #If offByFactors is False, the DIFFERENCE between two consecutive 
    #detractors to equal "detractorsOffBy"

#########################################################################
############ There is no need to edit beyond this point           ######
#########################################################################    
    questionString+=createFootnote(author,attribution)
    questionString+=makeAnswers(prefix2answer, answer2question,
            units2answer,detractorsOffBy,offByFactors)
    return[questionString,prefix2answer,answer2question,
           units2answer,insertImage,author,attribution,about]
finishLatex() #This last dedented line is the program!
