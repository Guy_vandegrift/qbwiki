##Reads the tex file in ./bank, counts the questions and
##creates ./input/template.cvs that can be modified to
##create tests based on questions in the bank.

##Code requires ./bank and ./input/template directories
import os, sys, csv, re #(re not used yet)

global DIAGNOSTIC
DIAGNOSTIC=True

def getNames(): #returns [quizNames,testName])
    testName="template"
    quizNames=[]
    availableTests=[]
    texset=set()
 #collect set of tex files in '/bank'        
    for item in os.listdir('./bank'): 
        if item.endswith('.tex'):
            texset.add(item[:-4])
            quizNames.append(item[:-4])
    print('Updating template.csv in ./input')
    print(len(quizNames),' bankquizzes are available for',testName)    
    return [quizNames,testName]
#end def getNames

def getQuizInfo(quizName):
    #returns the quiztype and number of questions
    path2=os.path.join('./bank',quizName+".tex")
    with open(path2,'r') as fin:
        quizText=fin.read()
    #get quizType (spelled quiztype in latex docs)    
    start=quizText.find(r'\quiztype')
    str2search=quizText[start+10:start+25]
    if str2search.find("conceptual")==1:
        quiztype="conceptual"
    elif str2search.find("numerical")==1:
        quiztype="numerical"
    else:
        quiztype="????????????????"
        print('unknown quiztype in getQuizInfo')
        sys.exit()
    #get numberOfQuestions (aka Nquestions)  
    start=quizText.find(r'\begin{questions}')
    stop=quizText.find(r'\end{questions}',start)
    str2search=quizText[start:stop]
    seeking=r'\question '
    numberOfQuestions=count_occurances(str2search,seeking)
    return[quiztype, numberOfQuestions]
# end def getQuizInfo

def find_all(a_str, sub): #this is a generator?
# x=list(find_all('spam spam spam spam', 'spam'))
#     print(x) yields: [0, 5, 10, 15]
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        yield start
        start += len(sub)
        # use start += 1 to find overlapping matches
# end def find_all

def count_occurances(a_str, sub):
# count how often "sub" exists in "a_str"
    return len(  list( find_all(a_str, sub) )  )
# end count_occurances

######################  START PROGRAM  ########################
[quizNames,testName]=getNames()  
for quizName in quizNames:
    ss=[]#start spreadsheet
    row=[]

#Row 1 colums: A=number questions in Test, B=TestName,
#C=Sel column: user-selected number to be on test
#D=local banksize E=quizType, F=first question    
    row.append("=sum(C2:C"+str(1+len(quizNames))+")") #A
    row.append("x-name") #testName                    #B
    row.append("Sel")#Select number questions  test   #C
    row.append("=sum(D2:D"+str(1+len(quizNames))+")") #D    
    row.append('t')  #quiz type                       #E
    row.append(1)    #first question                  #F
    string="=if(max(g2:g"+str(1+len(quizNames))\
               +")>0, F1+1,0)"#logical if 
    row.append(string)#allows fillright of top row    #G             
    ss.append(row)
    bankQuestionCount=0
    for quizName in quizNames:
        [quizType, Nquestions]=getQuizInfo(quizName)
        bankQuestionCount+=Nquestions
        row=[]
        row.append(" ") #blank workspace           #A
        row.append(quizName)                       #B
        row.append(1)                              #C
        row.append(Nquestions)                     #D
        if quizType=="conceptual":                 #E
            row.append("c")
        else:
            row.append("n")
        for j in range(Nquestions):                #F
            row.append("1")                
        ss.append(row)   
path2='./input/template/'+"template"+'.csv'
with open(path2,'w',newline='') as fout:
    wr=csv.writer(fout,delimiter=',')
    for row in ss:
        wr.writerow(row)
print("The bank currently holds",bankQuestionCount,"questions")
print("Template update complete")
input('press enter to exit')

    
