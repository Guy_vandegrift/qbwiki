clc; format compact;clear,Csv=[];
%>Step 1: Enter author's name and attribution as strings:
AUTHOR='Guy vandegrift';
ATTRIBUTION='C00';%typically C00 (Public Domain) or CC-BY-SA
%>Step 2: Make an optional comment: Break line with '... 
ABOUT=['This attribution is for the question '...
    'The MatLab code that expresses this question is released '...
    'to the public domain.'];
%>Step 3: Enter  question, using __ before variables. 
QUESTION=  ['A mass of __m1 kg is attached to a mass of __m2 kg and '...
    'experiences a force of __f N.  What is the acceleration?'];
    %Warning: Do not use commas anywhere in the question.
%>Step 4: Enter variable strings, in same order as above.
    VARLISTSTRING={'m1','m2','f'};   
    %....................................... MUST MATCH BELOW 
    NVARIABLES=length(VARLISTSTRING);%NO TOUCH!
%>Step 5: Select the number of renditions (typically 3-20):
   NRENDITIONS=3; 
    for NN = [1:NRENDITIONS]
%>Step 6: Assign random variables and solve
    f = 1%randi([501 999])/10;
    m1 = 1%randi([101 199])/10;
    m2 = 1%randi([201 399])/10;
%>Step 7: Rewrite VariableList of step 4 and as variables (not string):
    VARLISTVARIABLE=[m1, m2, f];     
    %....................................... MUST MATCH ABOVE 
%>Step 8: Solve for ANSWER (caps denote "magic words")
    mTot=m1+m2;
    a= f /mTot;   
    ANSWER = a;  
%>Step 9: Enter units for the answer:
   UNITS='m/s/s';
% >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  
% !!!! DONE !!!! DONE !!!! !!!! DONE !!!! DONE !!!!
    CsvRow = [ANSWER VARLISTVARIABLE]; 
    Csv=[Csv;CsvRow];   
end
string=mfilename
fout=fopen([string '.csv'],'wt')

Spaces=[repmat(',',1,NVARIABLES+1)];
OutputText=[AUTHOR, Spaces,'\n'];
OutputText=[OutputText, ATTRIBUTION, Spaces,'\n'];
OutputText=[OutputText, ABOUT, Spaces,'\n'];
OutputText=[OutputText, QUESTION, Spaces,'\n'];

%Now we write the separation row:
string=UNITS;

for jj = [1:NVARIABLES]
ss=['__' VARLISTSTRING{jj}];
string=[string,',',ss];
end
OutputText=[OutputText,string,',\n']

for ii=[1:NRENDITIONS]
    Line='';
    for jj=[1:NVARIABLES+1]
        ss=num2str(Csv(ii,jj))
        Line=[Line ss ',']; 
    end
    Line=[Line '\n']
    OutputText=[OutputText Line];
end

fprintf(fout,OutputText);
fclose(fout)

string=mfilename;
Line=[string '\n']
Line=[Line 'Author: ' AUTHOR '\n']
Line=[Line 'Attribution: ' ATTRIBUTION '\n']
Line=[Line 'About: ' ABOUT '\n\n']
Line=[Line 'Renditions:\n\n']


for ii=[1:NRENDITIONS]
    Line=[Line num2str(ii) '. '];
    QuestionX=QUESTION
   for jj=[1:NVARIABLES]
        VariableTag=['__' VARLISTSTRING{jj} ];
        Vstring=num2str(Csv(ii,jj+1))
        QuestionX=strrep(QuestionX,VariableTag,Vstring)
   end
    Line=[Line QuestionX]
    Line=[Line '\n\n']
end
Line=[Line '\nAnswers\n']
for ii=[1:NRENDITIONS]
    Line=[Line num2str(ii) '. ' num2str(Csv(ii,1)) ' ' UNITS '\n'];
end
ftext=fopen([mfilename '.txt'],'w');
fprintf(ftext,Line)
fclose(ftext)

