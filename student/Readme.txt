This folder contains simple codes designed to allow students to use 
MatLab or Excel.  See the apples folder for a simple Python code.

The original files needed here are:
* fIsMaMatlab.m (creates a csv file of the same name)
* FisMaXL.xlxs (must be created and edited by Excel)
* numericalCsv.py
* Readme.txt
Later create:
  *fIsMaMatlab.csv (using the matlab code of the same name)
  *fisMaMatlab.tex and fisMaMatlab.txt (using python from IDLE)
  *If you have Latex, run fisMaMatlab.tex to get the pdf file..

   First run fIsMaMatlab.m to create fIsMaMatlab.csv (it has the same name as the Excel file, but was instead created with Matlab.
   FisMaXL.xlxs is an Microsoft Excel code that can be saved as a csv and used by numericalCsv.py to create identical FisMa.tex and FisMa.txt files that have latex script.  The latex does not require images.
   For some reason, the csv file created by Excel requires an extra set of commas created by the colum of * after the last variable.
   fIsMaMatlab.m is a matlab script that also creates the required csv file.  This matlab script can be modified by students with a basic knowledge of matlab.


	

