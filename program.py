##This code must be run in a folder that contains:
##    1. ./bank (contains .tex files for all bank wikiquizzes)
##    2. ./bank/images (images contain all the image files.
##    3. ./input/xxx.csv (where one or more csv files are situated)
##    4. ./output (where the four xxx.tex files will be created)
##    5. ./output/images (I currently requre to copies of the images)
##The extra images file (in output) is not required for this program to run,
## but is required to run the output xxx.tex files that are created. Also
## created in the output file is an empty ./output/xxx directory where
## the pdf files created by LaTex can be placed (I use Miktex)
##
##Also in the directory that holds this program (program.py) is another
## program called template.py.  Use template.py to create the xxx.csv files 
## used to create the xxx.csv files used by program.py.

import os, sys, csv, re, time, copy, shutil, random#(re not used yet)
global debugmode
debugmode=False
Nversions=5
statement='%statement\n'
statement+='Though posted on Wikiversity, this document was created without '
statement+='wikitex using Python to write LaTeX markup.  With a bit more development '
statement+='it will be possible for users to download and use software that will '
statement+='permit them to create, modify, and print their own versions of this document.'
##This code opens a testfile (e.g. anyTest.csv) and
##verifies that it is properly formatted.  It checks that:
##1. The testName is in the 01 cell of the csv file
##2. The number of questions in the bank in the 00 file is correct
##3. Each available question has a top row integral question number  
## If possible it fixes the error and creates anyTestUPGRADE.csv
####Code requires ./bank/ and ./input/template/ directories
## getInfo returns testName
## getCheckData returns csvData (what might be on test)
class Question:
    def __init__(self):
        self.quizName =""
        self.number=0 #
        self.quizType = ""
        self.QA = [] #question and answer
        self.Q=[] #question only
        self.A=[] #answer only
        self.Nrenditions = 1
class Quiz:
    def __init__(self):
        self.quizName =""
        self.quizType = ""
        self.questions = []#Question() clas list
        self.Nquestions=0#number of questions
class CsvInfo:
    def __init__(self):
        self.comments=[]#col 1a       
        self.quizNames=[]#col 2b
        self.selections=[]#col 3c (Sel)
        self.Nquestions=[]#col 4d (available)
        self.types=[]# t,n,c col 4e 
        self.choices=[] #list starts at 5f
def whatis(string, x):
    print(string+' value=',repr(x),type(x))
    return string+' value='+repr(x)+repr(type(x))
def getFromChoices(first,second,num2pick): #select num2pick items
    random.shuffle(first)#list of integers first priority
    random.shuffle(second)#list of integers second priority
    allofem=(first+second)

    Nmax=min(len(allofem),num2pick)#Don't pick more than available
    selected=allofem[0:Nmax]
    random.shuffle(selected)
    return selected
def getTestName():
    if debugmode: #set testName to sample and return
        return "s_ample"
    availableTests=[]
    for item in os.listdir('./input'):
        if item.endswith(".csv"):
            availableTests.append(item[:-4])
    print("availableTests: ", availableTests)  
    testName=input('enter testName ')   
    if testName not in availableTests:
        testName="s_ample"
        print("testName taken to be",testName)
        if testName not in availableTests:
            print(testName, " not available in ./input")
            sys.exit("testName not available in ./input")
    return testName
def getCsv(csvInfo,testName):
    string=os.path.join("./input",testName+'.csv')
    with open(string,newline='') as fin:
        reader = csv.reader(fin)
        csvData=list(reader)
    #rows & columns indexed as follows
    cvsRows=len(csvData)#number of rows: index as nrow
    cvsCols=len(csvData[0])#num cols: index as ncol
    #print('csvData dimensions: Nrow,Ncol',cvsRows,cvsCols)   
    commentColA=[]#0a comments used 
    quizNameColB=[]#1b quiznames list
    selectionColC=[]#2c num Sel=S=selected for test
    NavailableColD=[]#3d num questions in quiz (bank)
    typeColE=[]#4e quizType list
    choiceListF=[]#5f lists len(NQues) w/ 0,1,or 2
    ##The first row is different:
    ##0A=number of questions on test
    ##0B=testName
    ##0C="Sel" (either 0,1,2 where "blank" means 0
    ##0D=number of questions in the bank
    ##0E="t" (type)
    ##0F,0G,...=1,2,...          
    for nrow in range(0,cvsRows):
        commentColA.append(csvData[nrow][0])#0a
        quizNameColB.append(csvData[nrow][1])   #1b
        selectionColC.append(csvData[nrow][2])   #2c        
        NavailableColD.append(csvData[nrow][3])  #3d
        typeColE.append(csvData[nrow][4])   #4e
        choiceListF.append(csvData[nrow][5:cvsCols])
    csvInfo.comments=commentColA
    csvInfo.quizNames=quizNameColB
    csvInfo.selections=selectionColC
    csvInfo.Nquestions=NavailableColD
    csvInfo.types=typeColE
    csvInfo.choices=choiceListF        
    return [csvInfo,cvsRows,csvData]              #ends getCvs
def prefixUnderscore(string):
    string=string.replace('_','\\_')
    return string    
def findLatexWhole(pre,string,post):
    #returns list of indices between the pre and post
    #tags.  Whole includes the tags
    mylist=[]
    tag=pre+r'(.*?)'+post
    matches=re.finditer(tag,string,re.S)
    for item in matches:
        mylist.append([item.start(),item.end()])
    #mylist is a lists of two element lists (start/st
    #the two element list is the start/stop point in string
    #mylist is as long as there are instances of the tag
    return mylist  #ends findLatexWhole
def findLatexBetween(pre,strIn,post):
    #returns list of indeces between the pre and post
    #tags.  Between excludes the tags
    mylist=[]
    tag=pre+r'(.*?)'+post
    matches=re.finditer(tag,strIn,re.S)
    for item in matches:
    #To get "between" we subtract out the pre and post
    #part of the tag.  We need to count the backslashes
    #in order to compensate for the extra \ in the \\ escape
        n1=item.start()+len(pre)-pre.count(r'\\')
        n2=item.end()-len(post)+post.count(r'\\')#as before:
    #mylist is a lists of two element lists (start/st
    #the two element list is the start/stop point in string
    #mylist is as long as there are instances of the tag
        mylist.append([n1,n2])
    return mylist                        #ends findLatexBetween
def QA2QandA(string): 
    #Define 4 patterns:
    beginStr=r'\\begin{choices}'
    endStr=r'\\end{choices}'
    CoStr=r'\\CorrectChoice '
    chStr=r'\\choice '
    #get Q=question
    n=string.find('\\begin{choices}')
    Q=string[0:n]
    #get A=string of answers
    #newStr defines the new search parameter as 1st answer
    pat=CoStr+'|'+chStr+'|'+endStr+'(.*?)'#fails-not greedy   
    iterations=re.finditer(pat,string,re.S)
    nList=[0]
    A=[]  
    for thing in iterations:
        nList.append(thing.start())
    #for i in range(len(nList)): #crashes as expected
    for i in range(1,len(nList)-1):
        A.append(string[nList[i]:nList[i+1]])
    return [Q,A]
def makeQuiz(path,quizName):
    strIn=""
    with open(path,'r') as fin:
      for line in fin:
        strIn+=line
        ########### we build Quiz() etc from strIn
    quiz=Quiz()
    quiz.quizName=quizName  
    x=findLatexBetween(r'{\\quiztype}{',strIn,'}')
    quizType=strIn[x[0][0]:x[0][1]]
    quiz.quizType=quizType                      
    x=findLatexBetween(r'\\begin{questions}',  #List of index pairs
                    strIn,r'\\end{questions}') #(start,stop)
    firstBatch=strIn[x[0][0]:x[0][1]]    #not sure I need x and y
    y=findLatexWhole(r'\\question ',firstBatch, r'\\end{choices}')
    quiz.Nquestions=len(y)#=number of questions in quiz    
    for i in range(quiz.Nquestions):                
        question=Question()
        question.QA=[]#I don't know why, but code ran without this
        question.Q=[]#ditto
        question.A=[]#ditto
        question.quizName=quizName
        question.number=i+1
        question.quizType=quizType
        #Now we search firstBatch for the i-th QA
        #(where QA is the "Question&Answers" string)
        questionAndAnswer=firstBatch[y[i][0]:y[i][1]]
        question.QA.append(questionAndAnswer)
        [Q0,A0]=QA2QandA(questionAndAnswer)
        question.Q.append(Q0)
        question.A.append(A0)
        #more needs to be added if numerical        
        #Also need questionQ an question.A
        quiz.questions.append(question)
    if question.quizType=="numerical":
        z=findLatexBetween(r'\\section{Renditions}',
                  strIn,r'\\section{Attribution}')        
        renditionsAll=strIn[z[0][0]:z[0][1]]
        w=findLatexWhole(r'\\begin{questions}',renditionsAll,
                         r'\\end{questions}')
        for i in range(quiz.Nquestions):
            #reopen each question from quiz
            question=quiz.questions[i]
            renditionsThis=renditionsAll[w[i][0]:w[i][1]]
            v=findLatexWhole(r'\\question ',renditionsThis,
                             r'\\end{choices}')
            question.Nrenditions=len(v)
            for j in range(len(v)):
                QAj=renditionsThis[v[j][0]:v[j][1]]
                 #########  fix pagebreak bug ################                             
                QAj=QAj.replace('\\pagebreak',' ')
                question.QA.append(QAj)
                question.Q.append(QAj)
                question.A.append(QAj)
    return quiz #ends MakeQuiz
def startLatex(titleTxt,quizType,statement,timeStamp): #this starts the Latex markup
    titleTex=titleTxt.replace('_','\\_')
    string=r'''%PREAMBLE
\newcommand{\quiztype}{'''+quizType+r'''}
\newif\ifkey\documentclass[11pt,twoside]{exam}
\RequirePackage{amssymb, amsfonts, amsmath, latexsym, verbatim,
xspace, setspace,datetime,tikz, pgflibraryplotmarks, hyperref}
\usepackage[left=.4in, right=.4in, bottom=.9in, top=.7in]{geometry}
\usepackage{endnotes, multicol,textgreek,graphicx} \singlespacing 
\parindent 0ex \hypersetup{ colorlinks=true, urlcolor=blue}
\pagestyle{headandfoot}
\runningheader{'''+titleTex+r'''}{\thepage\ of \numpages}{'''\
+timeStamp+r'''}
\footer{}
{\LARGE{The next page might contain more answer choices for this question}}{}
% BEGIN DOCUMENT 
\begin{document}\title{'''+titleTex+r'''}
\author{\includegraphics[width=0.10\textwidth]
{images/666px-Wikiversity-logo-en.png}\\
The LaTex code that creates this quiz is released to the Public Domain\\
Attribution for each question is documented in the Appendix\\
\url{https://bitbucket.org/Guy_vandegrift/qbwiki/wiki/Home}\\
\url{https://en.wikiversity.org/wiki/Quizbank} \\
'''+quizType+r''' quiz '''+timeStamp+\
r'''}
\maketitle
'''+statement+"\n\\tableofcontents\n"
    return string
def AllStudyLatex(testName,statement,timeStamp,bnkQuizzes):
    bigStrAll=startLatex(testName+": All","mixed",statement,timeStamp)
    bigStrStudy=startLatex(testName+":Study","mixed",statement,timeStamp)
    NbnkQuiz=len(bnkQuizzes)
    for i in range(NbnkQuiz):
        quiz=bnkQuizzes[i]
        quizNameLatex=prefixUnderscore(quiz.quizName)    
        bigStrAll+=r'\section{'+quizNameLatex\
            +r'}\keytrue\printanswers'
        bigStrStudy+=r'\section{'+quizNameLatex\
            +r'}\keytrue\printanswers'
        #print zeroth renditions
        bigStrAll+='\n\\begin{questions}\n'
        bigStrStudy+='\n\\begin{questions}\n' 
        for j in range(quiz.Nquestions):
            thisQA=quiz.questions[j].QA
            bigStrAll+=thisQA[0]
            bigStrStudy+=thisQA[0]
        bigStrAll+='\n\\end{questions}\n'
        bigStrStudy+='\n\\end{questions}\n'
        if(quiz.questions[j].quizType)=='numerical':
            bigStrAll+='\n \\subsection{Renditions}'     
            for j in range(quiz.Nquestions): #iterates questions not renditions 
                bigStrAll+='\n\\subsubsection*{'+quizNameLatex+' Q'+str(j+1)+'}'
                thisQA=quiz.questions[j].QA
                bigStrAll+='\n\\begin{questions}\n'            
                for k in range(1,len(thisQA)): #A null loop if conceptual
                    bigStrAll+=thisQA[k]
                bigStrAll+='\n\\end{questions}\n'
    bigStrAll+='\n\\section{Attribution}\\theendnotes\n\\end{document}'
    bigStrStudy+='\n\\section{Attribution}\\theendnotes\n\\end{document}'
    #Make output
    path2=os.path.join('.\output',timeName)
    if not os.path.exists(path2):
        os.makedirs(path2)
    pathAll=os.path.join('.\output','All'+timeName+'.tex')
    with open(pathAll,'w') as latexOut:
        latexOut.write(bigStrAll)
    pathStudy=os.path.join('.\output','Study'+timeName+'.tex')
    with open(pathStudy,'w') as latexOut:
        latexOut.write(bigStrStudy)
    return
def checkcsvData(csvData, bnkQuizzes):
    csvChecks=True
    csvNew=copy.deepcopy(csvData)
    NquestionsMax=0 #will grow to the most questions on any wikiquiz
    NquestionsOnTest=0 #This will sum up the selections column 3c
    NquestionsInBnk=0#number questions in (local) bank
    #skip the top row of csvDat for now:
    Nquizzes=len(bnkQuizzes)
    for i in range(Nquizzes):       
        #col 1a comments. No need to check (except for top row).
        #col 2b testname
        thing=bnkQuizzes[i].quizName
        ss=csvData[i+1][1]   
        if repr(thing)!=repr(ss):
            print(thing+', '+ss+',quizname mismatch fatal error')
            sys.exit()
            csvChecks=False
        #col 3c Sel=number selected for test. No need to check.
        NquestionsOnTest+=int(csvData[i+1][2])
        #col 4d number questions available from bank
        thing=bnkQuizzes[i].Nquestions
        NquestionsInBnk+=thing
        if thing>NquestionsMax:
            NquestionsMax=thing
        ss=csvData[i+1][3]
        if str(thing)!=str(ss):
            print('thing', repr(thing), 'ss', repr(ss))
            print(str(thing),ss,'available questions mismatch')
            csvNew[i+1][3]=thing
            csvChecks=False
        #col 5e t=type
        thing=bnkQuizzes[i].quizType
        ss=csvData[i+1][4]
        if thing=='conceptual':
            thing='c'
        if thing=='numerical':
            thing='n'
        if thing!=ss:
            print(thing,ss,'quiztype mismatch')
            csvNew[i+1][4]=str(thing)
            csvChecks=False
    #col 6f  1 (2,3,...) were obtained from the ss=spreadsheet
    #Now do the first row:  Now thing has to be calculated
    #col 1a thing is the number of questions on the Test
    ss=csvData[0][0] #col 1a has index 0
    thing=NquestionsOnTest
    if str(thing)!=str(ss):
        print(repr(thing),repr(ss),'Questions on Test mismatch')
        csvNew[0][0]=str(thing)
        csvChecks=False
    ss=csvData[0][1] #col 2b is testName index 1
    thing=testName
    if str(thing)!=str(ss):
        print(thing,ss,'TestName mismatch')
        csvNew[0][1]=thing
        csvChecks=False
    ss=csvData[0][2] #col 3c Sel selection label index 2
    thing='Sel'
    if str(thing)!=str(ss):
        print(thing,ss,'Sel=selection col header not convention')
        csvNew[0][2]=thing
        csvChecks=False
    ss=csvData[0][3]
    thing=str(NquestionsInBnk)
    if str(thing)!=str(ss):
        print(thing,ss,'questions in (local) bank mismatch')
        csvNew[0][3]=str(thing)
        csvChecks=False
    ss=csvData[0][4] #col 5e is 't' type
    thing='t'
    if str(thing)!=str(ss):
        print(thing,ss,"needs 't' in top of column")
        csvNew[0][4]=thing
        csvChecks=False
    ss=csvData[0][5:len(csvData[0])]
    problemLabels=map(str,list(range(1,NquestionsMax+1)))
    thing=list(problemLabels)
    if str(thing)!=repr(ss):
        for n in range(0,NquestionsMax):
            csvNew[0][5+n]=str(n+1)
        csvChecks=False
    if csvChecks:
        print('csv file seems correct')
    else:
        print('csv file needs UPGRADE - upgrading now')
        upgradePath="./input/"+testName+'UPGRADE.csv'
        with open(upgradePath,'w',newline='') as fout:
            wr=csv.writer(fout,delimiter=',')
            for row in csvNew:
                wr.writerow(row)
    return csvChecks
def instructorLatex(testName,statement,timeStamp,bnkQuizzes,csvInfo):    
    bigStr=startLatex(testName+": Instructor","mixed",statement,timeStamp)
    bigStr+=r'\keytrue\printanswers'
    choiceList=[]
    for i in range(len(bnkQuizzes)):
        bigStr+=r'%New bank wikiquiz\\'
        bigStr+='\n'
        quiz=bnkQuizzes[i]
        quizName=quiz.quizName
        #must upgrade quizName to Latex format:
        quizName=quizName.replace('_','\\_')
        Nquestions=quiz.Nquestions        
        num2pick_str=csvInfo.selections[i+1]
        bigStr+=r'\subsection{'+num2pick_str
        bigStr+=' of '+ str(Nquestions) +' questions from '+quizName+'}\n'
        first=[]
        second=[]
        #choiceSel is the decision made regarding each question
        #in the wikiquiz: 0, 1, 2, or if outide range:''
        choiceSel=csvInfo.choices[i+1]
        #convert this list of "integer strings" to a string:
        bigStr+='\nSel: '
        bigStr+=', '.join(choiceSel)+r'.\\'
        iMax=len(choiceSel)
        for j in range(iMax):#note that the list starts at 1 not 0
            if choiceSel[j]=='1':
                first.append(int(j+1))
            if choiceSel[j]=='2':
                second.append(int(j+1))
        #randChoice selects randomly from this:
        randChoice=getFromChoices(first,second,int(num2pick_str))
        randChoice=sorted(randChoice)
        #create a printable string to show the random selection:
        bigStr+='\nRandom: '
        bigStr+=', '.join(map(str,randChoice))+r'.\\'+'\n'
        #choiceList is a list of lists:
        choiceList.append(randChoice)
        #all versions use these same choices and choice list saves
        #Review: i indexed the quizzes, so we let j index the questions
        bigStr+='\n'+'\\begin{questions}\n'
        for j in range(len(randChoice)):            
            jQ=randChoice[j]
            #print('randomChoice=jQ',jQ,'bnkquiz#=i',i)
            #recall that humans count questions beginning at 1
            #but python starts at zero:  jQ goes to jQ-1
            bigStr+=quiz.questions[jQ-1].QA[0]
        bigStr+='\n\\end{questions}\n'   
    bigStr+='\n\\section{Attribution}\n'
    bigStr+='Some attributions are located elsewhere.'
    bigStr+=r'''\endnote{The current system neglects to repeat the
attributions for the multipe renditions}'''
    bigStr+='\n\\theendnotes\n\\end{document}'        
    pathInstructor=os.path.join('.\output','Instructor'+timeName+'.tex')
    with open(pathInstructor,'w') as latexOut:
        latexOut.write(bigStr)
    return choiceList
def choiceList2string(choiceList,bnkQuizzes): #wherewasi
    #returns random selections from the choiceList in randomized order
    testQAlist=[]
    for i in range(len(bnkQuizzes)):
        quiz_i=bnkQuizzes[i]
        for j in range(len(choiceList[i])):
            choice_j=choiceList[i][j]
            jPy=choice_j-1#Python iterates from zero
            #choice_j is an integer
            question=quiz_i.questions[jPy]
            if question.quizType=="numerical":
                NR=question.Nrenditions
                nR=random.choice(range(1,NR))
                testQAlist.append(question.QA[nR])
            if question.quizType=="conceptual":
                Q=question.Q[0]
                Araw=question.A[0]
                Ashuffled=question.A[0]
                random.shuffle(Ashuffled)
                string=Q+'\\begin{choices}'
                for answer in Ashuffled:
                    string+=answer
                string+='\\end{choices}\n'
                #testQAlist.append(Q+Ashuffled)
                testQAlist.append(string)    
    random.shuffle(testQAlist)
    bigStr=''
    for item in testQAlist:
        bigStr+=item+'\n'
    return bigStr

def testLatex(testName,statement,timeStamp,bnkQuizzes,choiceList,
              nVersions):
    questionList=[]
    for n in range(len(bnkQuizzes)):
        quiz=bnkQuizzes[n]
        string=quiz.quizName+' ('+str(quiz.Nquestions)
        string+=' '+quiz.quizType+') choices: '
        string+=', '.join(map(str,choiceList[n]))
        #print(string)
        for quesNumStr in choiceList[n]:
            #pyQuesIndex gets us the question we want:
            pyQuesIndex=int(quesNumStr)-1
            question=quiz.questions[pyQuesIndex]
            questionList.append(question)
    bigStr=startLatex(testName+": Test","mixed",statement,timeStamp)
    #startLatex fixes the prefixUnderscore, but henceforth we need:
    testNameLatex=prefixUnderscore(testName)
    bigStr+='text before first subsection\n'
    for nver in range(nVersions):
        #noanswers part:
        bigStr+='\\cleardoublepage\\subsection{V'+str(nver)+'}\n'
        bigStr+='\\noprintanswers\\keyfalse\n'
        bigStr+='\\begin{questions}\n'
        testQAlist= choiceList2string(choiceList,bnkQuizzes)
        bigStr+=testQAlist       
        bigStr+='\\end{questions}\n'

        ##KEY part
        bigStr+='\\cleardoublepage\\subsubsection{KEY V'+str(nver+1)+'}\n'
        bigStr+='\\printanswers\\keyfalse\n'
        bigStr+='\\begin{questions}\n'
        bigStr+=testQAlist         
        bigStr+='\\end{questions}\n'    
    bigStr+='\n\\section{Attribution}\n'
    bigStr+='Some attributions are located elsewhere.'
    bigStr+=r'''\endnote{The current system neglects to repeat the
attributions for the multipe renditions}'''
    bigStr+='\n\\theendnotes\n\\end{document}'  
    pathTest=os.path.join('.\output','Test'+timeName+'.tex')
    with open(pathTest,'w') as latexOut:
        latexOut.write(bigStr)
    return 
####################   Program starts here ##########################
timeStamp=str(int(time.time()*100))
if debugmode==True:
    testName='s_ample'
    timeStamp='0000000' #and testName will be set to sample
    path2empty=os.path.join('.\output','0000000-sample')
    if os.path.exists(path2empty):
        shutil.rmtree(path2empty)
## This last line was an attempt to fix a mysterious failure.  I need
##        to remove path2empty because it is created later.
else:
    testName=getTestName()
##timeStamp=str(int(time.time()*100))
timeName = timeStamp+'-'+testName
path='./bank/'+testName+'.csv'
csvInfo=CsvInfo()
[csvInfo,cvsRows,csvData]=getCsv(csvInfo,testName)
#bnkQuizNames is a list of quznames in the bank
bnkQuizNames=csvInfo.quizNames[1:cvsRows]
bnkQuizzes=[]#bnkQuizzes is a list of objects of class Quiz()
for name in bnkQuizNames:
    bnkQuizzes.append(makeQuiz('./bank/'+name+'.tex',name))
csvChecks=checkcsvData(csvData, bnkQuizzes)
AllStudyLatex(testName,statement,timeStamp,bnkQuizzes)
choiceList=instructorLatex(testName,statement,timeStamp,bnkQuizzes,csvInfo)
testLatex(testName,statement,timeStamp,bnkQuizzes,choiceList,
          Nversions)
input('Type any key to exit program.py')
    



